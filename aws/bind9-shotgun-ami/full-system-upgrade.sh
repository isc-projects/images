#!/bin/sh
set -o xtrace -o nounset -o errexit -o pipefail

# initial pacman setup using script from AMI
sudo /etc/pacman.d/pacman_init.sh

# fetch package database and do full system upgrade
sudo pacman --noconfirm -Syu
