# SPDX-License-Identifier: MPL-2.0
"""AWS plugin: Configure IPv6 addresses from the beginning of delegated IPv6 prefix"""

from logging import Logger
import ipaddress
import itertools
import json
import subprocess

from cloudinit.cloud import Cloud
from cloudinit.config import Config
from cloudinit.config.schema import MetaSchema, get_meta_doc
from cloudinit.distros import ALL_DISTROS
from cloudinit.settings import PER_ALWAYS

IPV6_ADDR_COUNT = 256  # configure this many IPv6 addresses

meta: MetaSchema = {
    "id": "cc_extra_ipv6_prefix",
    "name": "Add IPv6 addresses from delegated prefixes",
    "title": "Configure additional IPv6 addresses from beginning of delegated prefixes",
    "description": __doc__,
    "distros": [ALL_DISTROS],
    "frequency": PER_ALWAYS,
    "examples": [],
    "activate_by_schema_keys": [],
}

__doc__ = get_meta_doc(meta)


def run_ip(args):
    return subprocess.check_output(["ip"] + args)


def run_ip_json(args):
    txt = run_ip(["--json"] + args)
    return json.loads(txt)


def get_mac_to_ifnames():
    interfaces = run_ip_json(["link"])
    mac_map = {}
    for interf in interfaces:
        # loopback and point-to-point stuff
        if interf["link_type"] == "loopback":
            continue
        try:
            mac = interf["address"]
        except KeyError:
            continue
        mac_map[mac] = interf["ifname"]

    return mac_map


def add_addrs(subnet, how_many, ifname):
    for addr in itertools.islice(ipaddress.ip_network(subnet).hosts(), how_many):
        run_ip(["address", "add", "dev", ifname, str(addr), "nodad", "noprefixroute"])


def as_list(value):
    if isinstance(value, list):
        return value
    return [value]


def handle(name: str, cfg: Config, cloud: Cloud, log: Logger, args: list) -> None:
    log.debug("cloud.datasource.metadata: %s", cloud.datasource.metadata)
    mac2name = get_mac_to_ifnames()
    log.debug("MAC => ifname map: %s", mac2name)
    for mac, ifconf in cloud.datasource.metadata["network"]["interfaces"][
        "macs"
    ].items():
        ifname = mac2name[mac]
        for prefix in as_list(ifconf.get("ipv6-prefix", [])):
            log.info(
                "delegated IPv6 prefix %s configured for interface %s, adding addresses",
                prefix,
                ifname,
            )

            add_addrs(prefix, IPV6_ADDR_COUNT, ifname)
            log.info(
                "delegated IPv6 prefix %s provisioned: addresses are set up on interface %s",
                prefix,
                ifname,
            )


# vi: ts=4 expandtab
