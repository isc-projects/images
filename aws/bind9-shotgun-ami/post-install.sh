#!/bin/sh
set -o xtrace -o nounset -o errexit -o pipefail

pacmanS() {
	sudo pacman --noconfirm -S --needed "$@"
}

# automation tools required by https://gitlab.nic.cz/knot/resolver-benchmarking
pacmanS git podman podman-docker docker-compose python-docker ethtool perf rsync curl python-pip python-setuptools unzip

# make Podman behave a bit closer to Docker
sudo cp -v /tmp/containers.cgroups.conf /etc/containers/containers.conf.d/
sudo cp -v /tmp/containers.limits.conf /etc/containers/containers.conf.d/
# allow user containers to use the same limits as well
sudo mkdir -v /etc/systemd/system.conf.d
sudo cp -v /tmp/systemd.limits.conf /etc/systemd/system.conf.d/

# make docker daemon available
sudo systemctl enable --now podman.socket

# /pcap mount point - a new volume will be provided externally when launching an instance
sudo cp -v /tmp/cloud.cfg.d.01_temp_disk.cfg /etc/cloud/cloud.cfg.d
# automount prevents failures and race conditions on boot
# unconditional mount would not work on server instances without PCAP partition
sudo mkdir -v /pcap
echo 'LABEL=pcap	/pcap	auto	rw,noatime,nobarrier,commit=3600,data=writeback,nofail,x-systemd.automount,x-systemd.device-timeout=1ms	0 0' | sudo tee -a /etc/fstab

# install custom cloud-init module
# assign IP addresses to the first 256 addresses in IPv6 prefix delegated to us
sudo cp -v /tmp/cloud.cfg.d.99_ipv6_prefix.cfg /etc/cloud/cloud.cfg.d
sudo cp -v /tmp/cc_extra_ipv6_prefix.py "$(python -c 'import cloudinit.config; print(cloudinit.config.__path__[0])')/"

# enable cgroups v2 for precise resource monitoring
echo 'GRUB_CMDLINE_LINUX_DEFAULT="$GRUB_CMDLINE_LINUX_DEFAULT systemd.unified_cgroup_hierarchy=1 audit=0"' | sudo tee -a /etc/default/grub
sudo grub-mkconfig | sudo tee /boot/grub/grub.cfg

# Amazon ECR and S3 API
pacmanS python-boto3

# AWS CLI
pacmanS aws-cli

# pikaur - helper for dnsjit installation
pacmanS base-devel git
cd /tmp
git clone https://aur.archlinux.org/pikaur.git
cd pikaur
makepkg -fsricL --noconfirm

pikaur -S --needed --noconfirm dnsjit

# mergecap
pacmanS wireshark-cli

# support for pspacek's and tkrizek's favorite terminal
pacmanS kitty-terminfo

# preinstall resource-monitor dependencies to save some time
pip install --break-system-packages -r https://gitlab.isc.org/isc-projects/resource-monitor/-/raw/main/requirements.txt

# use AWS-provided NTP servers
sudo mkdir -v /etc/systemd/timesyncd.conf.d/
sudo cp -v /tmp/timesyncd.conf /etc/systemd/timesyncd.conf.d/

# disable all timers; we do not timer to fire in the middle of a benchmark
systemctl show -p Id *.timer | grep Id= | cut -d = -f 2- | xargs sudo systemctl mask

# no other name-services to avoid suspicious conflicts
sudo systemctl stop systemd-resolved
sudo systemctl mask systemd-resolved
sudo rm -fv /etc/resolv.conf
sudo cp -v /tmp/resolv.conf /etc/resolv.conf

# disable persistent log storage
sudo mkdir -v /etc/systemd/journald.conf.d
sudo cp -v /tmp/journald.conf /etc/systemd/journald.conf.d
sudo systemctl disable systemd-journal-flush.service
sudo systemctl mask systemd-journal-flush.service
sudo systemctl disable systemd-update-utmp.service
sudo systemctl mask systemd-update-utmp.service

# AWS configuration
REGION=$(cloud-init query region)
aws configure set default.region "$REGION"
sudo aws configure set default.region "$REGION"

# login to ECR to cache images from registry
pikaur -S --needed --noconfirm amazon-ecr-credential-helper
sudo mkdir -v ~root/.docker
sudo cp -v /tmp/docker-config.json ~root/.docker/config.json
