#!/usr/bin/bash
set -o xtrace -o nounset -o errexit -o pipefail

find ~/.cache -type f -delete

sudo find ~root/.cache -type f -delete
sudo find ~root/.ecr -type f -delete

# --noconfirm does not work for -Scc
yes | pikaur -Scc || : ignore pipefail from yes command
sudo find /var/cache/pacman -type f -delete

sudo cloud-init clean --machine-id --logs
